﻿namespace Assignment_03_NET.Exceptions
{
    public class CharacterNotFoundException : Exception
    {
        public CharacterNotFoundException() : base("Character not found with that Id.")
        {

        }
    }
}
