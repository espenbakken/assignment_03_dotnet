﻿namespace Assignment_03_NET.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string? message) : base(message)
        {

        }
    }
}
