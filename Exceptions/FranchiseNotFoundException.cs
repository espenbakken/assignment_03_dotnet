﻿namespace Assignment_03_NET.Exceptions
{
    public class FranchiseNotFoundException : EntityNotFoundException
    {
        public FranchiseNotFoundException() : base("Franchise not found with that Id")
        {

        }
    }
}
