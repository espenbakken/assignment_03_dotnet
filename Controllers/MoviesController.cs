﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_03_NET.Models;
using Assignment_03_NET.Models.DTOs.Movies;
using Assignment_03_NET.Models.DTOs.Characters;
using Assignment_03_NET.Services.Movies;
using Assignment_03_NET.Exceptions;
using AutoMapper;
using System.Net;

namespace Assignment_03_NET.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        #region Get all movies
        /// <summary>
        /// Gets all movies from database async
        /// </summary>
        /// <returns></returns>
        [HttpGet("api/movies/")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetAllMovies()
        {
            return Ok
                (
                    _mapper.Map<List<MovieDTO>>(
                       await _movieService.GetAllAsync())
                );
        }
        #endregion

        #region Get movie by id
        /// <summary>
        /// Get movie from database based on its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Movie</returns>
        [HttpGet("api/movies/{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            try
            {
                return Ok(_mapper.Map<MovieDTO>(
                        await _movieService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        #endregion

        #region Update movie by id 
        /// <summary>
        /// Updates a movie by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>No content if success.</returns>
        [HttpPut("api/movies/{id}")]
        public async Task<IActionResult> PutMovieAsync(int id, MoviePutDTO movie)
        {
            if (id != movie.Id)
                return BadRequest();

            try
            {
               await _movieService.UpdateAsync(
                        _mapper.Map<Movie>(movie)
                        );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion

        #region Post new movie
        /// <summary>
        /// Post new movie to database
        /// </summary>
        /// <param name="moviePostDTO"></param>
        /// <returns></returns>
        [HttpPost("api/movies/")]
        public async Task<IActionResult> PostMovieAsync(MoviePostDTO moviePostDTO)
        {
            Movie movie = _mapper.Map<Movie>(moviePostDTO);
            await _movieService.AddAsync(movie);
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }
        #endregion

        #region Delete a movie by id
        /// <summary>
        /// Delete a movie in database by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("api/movies/{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            try
            {
                await _movieService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion

        #region Get all characters in movie
        /// <summary>
        /// Gets all characters in a movie. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("api/movies/{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterSummaryDTO>>> GetCharactersForMovieAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<CharacterSummaryDTO>>(
                            await _movieService.GetAllCharactersAsync(id)
                            )
                        );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion

        #region Update characters for a specific movie
        /// <summary>
        /// Updates characters for a given movie by its id.
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("api/movies/{id}/characters")]
        public async Task<IActionResult> UpdateCharactersForMovieAsync(int[] characterIds, int id)
        {
            try
            {
                await _movieService.UpdateCharactersAsync(characterIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        #endregion
    }
}
