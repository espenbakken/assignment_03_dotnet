﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_03_NET.Models;
using Assignment_03_NET.Models.DTOs.Characters;
using Assignment_03_NET.Services.Characters;
using Assignment_03_NET.Exceptions;
using AutoMapper;
using System.Net;

namespace Assignment_03_NET.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharacterController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        #region Get all characters
        /// <summary>
        /// Gets all characters from database async
        /// </summary>
        /// <returns>All characters</returns>
        [HttpGet("api/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetAllCharacters()
        {
            return Ok
                (
                    _mapper.Map<List<CharacterDTO>>(
                        await _characterService.GetAllAsync())
                );
        }
        #endregion

        #region Get one character by id
        /// <summary>
        /// Gets a character from database async based on its id.
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <returns>Character by id</returns>
        [HttpGet("api/characters/{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacter(int id)
        {
            try
            {
                return Ok(_mapper.Map<CharacterDTO>(
                        await _characterService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        #endregion

        #region Update character by id
        /// <summary>
        /// Lets you update a characters fields, based on its id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns>No content if success</returns>
        [HttpPut("api/characters/{id}")]
        public async Task<IActionResult> PutCharacterAsync(int id, CharacterPutDTO character)
        {
            if (id != character.Id)
                return BadRequest();

            try
            {
                await _characterService.UpdateAsync(
                        _mapper.Map<Character>(character)
                        );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion

        #region Post new character
        /// <summary>
        /// Post new character to database
        /// </summary>
        /// <param name="characterPostDTO"></param>
        /// <returns></returns>
        [HttpPost("api/characters/")]
        public IActionResult PostCharacter(CharacterPostDTO characterPostDTO)
        {
            Character character = _mapper.Map<Character>(characterPostDTO);
            _characterService.AddAsync(character);
            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }
        #endregion

        #region Delete a character by id
        /// <summary>
        /// Deletes character from database by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("api/characters/{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            try
            {
                await _characterService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion
    }
}
