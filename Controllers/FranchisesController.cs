﻿using Assignment_03_NET.Exceptions;
using Assignment_03_NET.Models.DTOs.Characters;
using Assignment_03_NET.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Assignment_03_NET.Models.DTOs.Franchises;
using Assignment_03_NET.Services.Franchises;
using Assignment_03_NET.Models.DTOs.Movies;

namespace Assignment_03_NET.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchiseController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        #region Get all franchises
        /// <summary>
        /// Gets all franchises from database async
        /// </summary>
        /// <returns>Franchises</returns>
        [HttpGet("api/franchise/")]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetAllFranchises()
        {
            return Ok
                (
                    _mapper.Map<List<FranchiseDTO>>(
                        await _franchiseService.GetAllAsync())
                );
        }
        #endregion

        #region Get one franchise by id
        /// <summary>
        /// Get one franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>One specific franchise</returns>
        [HttpGet("api/franchise/{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            try
            {
                return Ok(_mapper.Map<FranchiseDTO>(
                        await _franchiseService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        #endregion

        #region Update a franchise by its id
        /// <summary>
        /// Update a franchise by its id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns>NoContent if success</returns>
        [HttpPut("api/franchise/{id}")]
        public async Task<IActionResult> PutFranchiseAsync(int id, FranchisePutDTO franchise)
        {
            if (id != franchise.Id)
                return BadRequest();

            try
            {
                await _franchiseService.UpdateAsync(
                         _mapper.Map<Franchise>(franchise)
                         );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion

        #region Add a new franchise
        /// <summary>
        /// Adds a new franchise to database
        /// </summary>
        /// <param name="franchisePostDTO"></param>
        /// <returns></returns>
        [HttpPost("api/franchise/")]
        public async Task<IActionResult> PostFranchise(FranchisePostDTO franchisePostDTO)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchisePostDTO);
            await _franchiseService.AddAsync(franchise);
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }
        #endregion

        #region Delete a franchise
        /// <summary>
        /// Deletes a franchise from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("api/franchise/{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            try
            {
                await _franchiseService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion

        #region Get characters for franchise
        /// <summary>
        /// Gets all characters for a given franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All characters</returns>
        [HttpGet("api/franchise/{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharactersForFranchiseAsync (int id)
        {
            if (!await FranchiseExists(id))
            {
                return NotFound();
            }
            var characters = await _franchiseService.GetAllCharactersAsync(id);

            return Ok(characters.Select(c => _mapper.Map<CharacterDTO>(c)));
        }
        #endregion

        #region Get all movies for franchise
        /// <summary>
        /// Gets all movies for a given franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Movies</returns>
        [HttpGet("api/franchise/{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieSummaryDTO>>> GetMoviesForFranchiseAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<MovieSummaryDTO>>(
                            await _franchiseService.GetAllMoviesAsync(id)
                            )
                        );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        #endregion

        #region Update movies for a franchise
        /// <summary>
        /// Update movies for a given franchise.
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("api/franchise/{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForFranchiseAsync(int[] movieIds, int id)
        {
            try
            {
                await _franchiseService.UpdateMoviesAsync(movieIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        #endregion

        #region Check if franchise exist
        private async Task<bool> FranchiseExists(int id)
        {
            return await _franchiseService.FranchiseExistsAsync(id);
        }
        #endregion

    }
}
