﻿using Assignment_03_NET.Models;
using Assignment_03_NET.Models.DTOs.Franchises;
using AutoMapper;

namespace Assignment_03_NET.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchisePostDTO, Franchise>();
            CreateMap<FranchisePutDTO, Franchise>();

            CreateMap<Franchise, FranchiseDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.MovieTitle).ToList()));

            CreateMap<Franchise, FranchiseSummaryDTO>();
        }
    }
}
