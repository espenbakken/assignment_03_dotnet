﻿using AutoMapper;
using Assignment_03_NET.Models;
using Assignment_03_NET.Models.DTOs.Movies;


namespace Assignment_03_NET.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>()
                .ForMember(dto => dto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.Id).ToList()))
                .ForMember(dto => dto.Franchise, opt => opt
                .MapFrom(m => m.Franchise.Name));
            CreateMap<Movie, MovieSummaryDTO>();
            CreateMap<MoviePutDTO, Movie>();

            CreateMap<MoviePostDTO, Movie>();
        }
    }
}
