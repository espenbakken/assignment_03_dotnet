﻿using Assignment_03_NET.Models;
using Assignment_03_NET.Models.DTOs.Characters;
using AutoMapper;

namespace Assignment_03_NET.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterSummaryDTO>();
            CreateMap<CharacterPutDTO, Character>();
            CreateMap<CharacterPostDTO, Character>();
            CreateMap<Character, CharacterDTO>();
        }
    }
}
