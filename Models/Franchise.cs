﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment_03_NET.Models
{
    [Table("Franchise")]
    public partial class Franchise
    {
        public Franchise()
        {
            Movies = new HashSet<Movie>();
        }

        [Key]
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        //Navigation Properties
        public virtual ICollection<Movie>? Movies { get; set; }
    }
}
