﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment_03_NET.Models
{
    [Table("Character")]
    public partial class Character
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(100)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string? Alias { get; set; }

        [MaxLength(50)]
        public string Gender { get; set; }

        [MaxLength(256)]
        public string? Picture { get; set; }

        //Navigation Properties
        public virtual ICollection<Movie>? Movies { get; set; }

    }
}
