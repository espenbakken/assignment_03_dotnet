﻿namespace Assignment_03_NET.Models.DTOs.Characters
{
    public class CharacterPostDTO
    {
        public string FullName { get; set; } = null!;
        public string Alias { get; set; } = null!;
        public string Gender { get; set; } = null!;
        public string Picture { get; set; }
    }
}
