﻿namespace Assignment_03_NET.Models.DTOs.Characters
{
    public class CharacterSummaryDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; } = null!;
    }
}
