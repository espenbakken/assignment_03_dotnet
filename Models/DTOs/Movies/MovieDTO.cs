﻿
namespace Assignment_03_NET.Models.DTOs.Movies
{
    public class MovieDTO
    {
        public int Id { get; set; }

        public string MovieTitle { get; set; } = null!;

        public string Genre { get; set; } = null!;

        public int ReleaseYear { get; set; }

        public string Director { get; set; } = null!;

        public List<string> Characters { get; set; } = null!;
        public string Franchise { get; set; } = null!;

    }
}
