﻿namespace Assignment_03_NET.Models.DTOs.Movies
{
    public class MoviePutDTO
    {
        public int Id { get; set; }

        public string MovieTitle { get; set; } = null!;

        public string Genre { get; set; } = null!;

        public int? ReleaseYear { get; set; }

        public string Director { get; set; } = null!;
    }
}
