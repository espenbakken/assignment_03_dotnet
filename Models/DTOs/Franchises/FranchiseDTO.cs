﻿namespace Assignment_03_NET.Models.DTOs.Franchises
{
    public class FranchiseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public List<string> Movies { get; set; } = null!;
    }
}
