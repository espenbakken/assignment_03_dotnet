﻿namespace Assignment_03_NET.Models.DTOs.Franchises
{
    public class FranchisePostDTO
    {
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
    }
}
