﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment_03_NET.Models
{
    [Table ("Movies")]
    public partial class Movie
    {
        public Movie()
        {
            Characters = new HashSet<Character>();
        }

        [Key]
        public int Id { get; set; }

        [MaxLength(150)]
        public string MovieTitle { get; set; }

        [MaxLength(256)]
        public string Genre { get; set; }
        
        [MaxLength(10)]
        public int ReleaseYear { get; set; }
        
        [MaxLength(100)]
        public string Director { get; set; }

        [MaxLength(256)]
        public string? Picture { get; set; }

        [MaxLength(256)]
        public string? Trailer { get; set; }

        //Relationships
        public int? FranchiseId { get; set; }

        //Navigation Properties
        public virtual ICollection<Character> Characters { get; set; }
        public Franchise? Franchise { get; set; }
    }
}
