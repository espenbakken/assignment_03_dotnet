﻿using Assignment_03_NET.Context;
using Assignment_03_NET.Models;
using Microsoft.EntityFrameworkCore;
using Assignment_03_NET.Exceptions;

namespace Assignment_03_NET.Services.Characters
{
    public class CharacterServiceImpl : ICharacterService
    {
        private readonly MovieCharactersEfContext _movieCharactersEfContext;
        private readonly ILogger<CharacterServiceImpl> _logger;

        public CharacterServiceImpl(MovieCharactersEfContext movieCharactersDbContext, ILogger<CharacterServiceImpl> logger)
        {
            _movieCharactersEfContext = movieCharactersDbContext;
            _logger = logger;
        }

        public async Task AddAsync(Character entity)
        {
            await _movieCharactersEfContext.AddAsync(entity);
            await _movieCharactersEfContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var character = await _movieCharactersEfContext.Characters.FindAsync(id);
            if (character == null)
            {
                _logger.LogError("Character not found with Id: " + id);
                throw new CharacterNotFoundException();
            }
            _movieCharactersEfContext.Characters.Remove(character);
            await _movieCharactersEfContext.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetAllAsync()
        {
            return await _movieCharactersEfContext.Characters
                .ToListAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
            if(!await CharacterExistsAsync(id))
            {
                _logger.LogError("Character not found with " + id);
                throw new CharacterNotFoundException();
            }
            return await _movieCharactersEfContext.Characters
                .Where(c => c.Id == id)
                .FirstAsync();
        }

        public async Task UpdateAsync(Character entity)
        {
            if (!await CharacterExistsAsync(entity.Id))
            {
                _logger.LogError("Character not found with" + entity.Id);
                throw new CharacterNotFoundException();
            }
            _movieCharactersEfContext.Entry(entity).State = EntityState.Modified;
            await _movieCharactersEfContext.SaveChangesAsync();
        }

        private async Task<bool> CharacterExistsAsync(int id)
        {
            return await _movieCharactersEfContext.Characters.AnyAsync(m => m.Id == id);
        }
    }
}
