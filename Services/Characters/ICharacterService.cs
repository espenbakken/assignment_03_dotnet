﻿using Assignment_03_NET.Models;
namespace Assignment_03_NET.Services.Characters
{
    public interface ICharacterService : ICrudService<Character, int>
    {
    }
}
