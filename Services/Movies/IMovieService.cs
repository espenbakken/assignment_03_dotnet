﻿using Assignment_03_NET.Models;
namespace Assignment_03_NET.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        Task<ICollection<Character>> GetAllCharactersAsync(int movieId);
        Task UpdateCharactersAsync(int[] characterIds, int movieId);
        
    }
}
