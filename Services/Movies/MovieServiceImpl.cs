﻿using Assignment_03_NET.Context;
using Assignment_03_NET.Models;
using Microsoft.EntityFrameworkCore;
using Assignment_03_NET.Exceptions;

namespace Assignment_03_NET.Services.Movies
{
    public class MovieServiceImpl : IMovieService
    {
        private readonly MovieCharactersEfContext _movieCharactersDbContext;
        private readonly ILogger<MovieServiceImpl> _logger;

        public MovieServiceImpl(MovieCharactersEfContext movieCharactersEfContext, ILogger<MovieServiceImpl> logger)
        {
            _movieCharactersDbContext = movieCharactersEfContext;
            _logger = logger;
        }

        public async Task AddAsync(Movie entity)
        {
            await _movieCharactersDbContext.AddAsync(entity);
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var movie = await _movieCharactersDbContext.Movies.FindAsync(id);
            if (movie == null)
            {
                _logger.LogError("Movie not found with Id: " + id);
                throw new MovieNotFoundException();
            }
            _movieCharactersDbContext.Movies.Remove(movie);
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _movieCharactersDbContext.Movies
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .ToListAsync();
        }

        public async Task<ICollection<Character>> GetAllCharactersAsync(int id)
        {
          Movie? movie = await _movieCharactersDbContext.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
          if (!await MovieExistsAsync(id))
            {
                _logger.LogError("Movie not found with id: " + id);
                throw new MovieNotFoundException();
            }
            return movie.Characters.ToList();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            if(!await MovieExistsAsync(id))
            {
                _logger.LogError("Movie not found with" + id);
                throw new MovieNotFoundException();
            }
            return await _movieCharactersDbContext.Movies
                .Where(m => m.Id == id)
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .FirstAsync();
        }

        public async Task UpdateAsync(Movie entity)
        {
            if (!await MovieExistsAsync(entity.Id))
            {
                _logger.LogError("Movie not found with" + entity.Id);
                throw new MovieNotFoundException();
            }

            _movieCharactersDbContext.Entry(entity).State = EntityState.Modified;
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        public async Task UpdateCharactersAsync(int[] characterIds, int movieId)
        {
            if (!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Movie not found with" + movieId);
                throw new MovieNotFoundException();
            }
            List<Character> characters = characterIds
                .ToList()
                .Select(cid => _movieCharactersDbContext.Characters
                .Where(c => c.Id == cid).First())
                .ToList();
            Movie movie = await _movieCharactersDbContext.Movies
                .Where(m => m.Id == movieId)
                .FirstAsync();
            movie.Characters = characters;
            _movieCharactersDbContext.Entry(movie).State = EntityState.Modified;
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _movieCharactersDbContext.Movies.AnyAsync(m => m.Id == id);
        }
    }
}
