﻿using Assignment_03_NET.Models;
namespace Assignment_03_NET.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        Task<ICollection<Movie>> GetAllMoviesAsync(int franchiseId);
        Task<ICollection<Character>> GetAllCharactersAsync(int franchiseId);
        Task UpdateMoviesAsync(int[] movieIds, int franchiseId);
        Task<bool> FranchiseExistsAsync(int franchiseId);
    }
}
