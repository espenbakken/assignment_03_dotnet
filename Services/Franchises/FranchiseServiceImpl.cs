﻿using Assignment_03_NET.Context;
using Assignment_03_NET.Exceptions;
using Assignment_03_NET.Models;
using Microsoft.EntityFrameworkCore;

namespace Assignment_03_NET.Services.Franchises
{
    public class FranchiseServiceImpl : IFranchiseService
    {
        private readonly MovieCharactersEfContext _movieCharactersDbContext;
        private readonly ILogger<FranchiseServiceImpl> _logger;

        public FranchiseServiceImpl(MovieCharactersEfContext movieCharactersEfContext, ILogger<FranchiseServiceImpl> logger)
        {
            _movieCharactersDbContext = movieCharactersEfContext;
            _logger = logger;
        }
        
        public async Task AddAsync(Franchise entity)
        {
            await _movieCharactersDbContext.AddAsync(entity);
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var franchise = await _movieCharactersDbContext.Franchises.FindAsync(id);
            if (franchise == null)
            {
                _logger.LogError("Franchise not found with Id: " + id);
                throw new FranchiseNotFoundException();
            }
            _movieCharactersDbContext.Franchises.Remove(franchise);
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _movieCharactersDbContext.Franchises
                .Include(f => f.Movies)
                .ToListAsync();
        }

        public async Task<ICollection<Character>> GetAllCharactersAsync(int id)
        {
            /* Franchise? franchise = await _movieCharactersDbContext.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
             if (!await FranchiseExistsAsync(id))
             {
                 _logger.LogError("Franchise not found with id: " + id);
                 throw new FranchiseNotFoundException();
             }
             return null;*/
            var characters = new HashSet<Character>();
            var movies = await _movieCharactersDbContext.Movies.Include(m => m.Characters).Where(m => m.FranchiseId == id).ToListAsync();

            foreach(var movie in movies)
            {
                foreach (var character in movie.Characters)
                {
                    characters.Add(character);
                }
            }
            return characters;
        }

        public async Task<ICollection<Movie>> GetAllMoviesAsync(int id)
        {
            Franchise? franchise = await _movieCharactersDbContext.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            if (!await FranchiseExistsAsync(id))
            {
                _logger.LogError("Franchise not found with id: " + id);
                throw new FranchiseNotFoundException();
            }
            return franchise.Movies.ToList();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            if(!await FranchiseExistsAsync(id))
            {
                _logger.LogError("Franchise not found with Id: " + id);
                throw new FranchiseNotFoundException();
            }
            return await _movieCharactersDbContext.Franchises
                .Where(f => f.Id == id)
                .Include(f => f.Movies)
                .FirstAsync();
        }

        public async Task UpdateAsync(Franchise entity)
        {
            if (!await FranchiseExistsAsync(entity.Id))
            {
                _logger.LogError("Franchise not found with Id: " + entity.Id);
                throw new FranchiseNotFoundException();
            }
            _movieCharactersDbContext.Entry(entity).State = EntityState.Modified;
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        public async Task UpdateMoviesAsync(int[] movieIds, int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Franchise not found with Id: " + franchiseId);
                throw new FranchiseNotFoundException();
            };
            List<Movie> movies = movieIds
                .ToList()
                .Select(mid => _movieCharactersDbContext.Movies
                .Where(m => m.Id == mid).First())
                .ToList();
            Franchise franchise = await _movieCharactersDbContext.Franchises
                .Where(f => f.Id == franchiseId)
                .FirstAsync();
            franchise.Movies = movies;
            _movieCharactersDbContext.Entry(franchise).State = EntityState.Modified;
            await _movieCharactersDbContext.SaveChangesAsync();
        }

        public async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _movieCharactersDbContext.Franchises.AnyAsync(f => f.Id == id);
        }
    }
}
