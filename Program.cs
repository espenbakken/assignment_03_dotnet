using Assignment_03_NET.Context;
using Assignment_03_NET.Services.Franchises;
using Assignment_03_NET.Services.Movies;
using Assignment_03_NET.Services.Characters;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;


var builder = WebApplication.CreateBuilder(args);

//Configuring generated XML docs for Swagger

var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "MovieCharacters Api",
        Description = "Simple API to manage a collection of movies.",
        Contact = new OpenApiContact
        {
            Name = "Espen Bakken & Kristian Stoa Mathisen",
            Url = new Uri("https://gitlab.com/espenbakken")
        },
        License = new OpenApiLicense
        {
            Name = "MIT 2022",
            Url = new Uri("https://opensource.org/licenses/MIT")
        }
    });
    options.IncludeXmlComments(xmlPath);
});

builder.Services.AddDbContext<MovieCharactersEfContext>(
    opt => opt.UseSqlServer(
        builder.Configuration.GetConnectionString("MovieCharactersDb")
        )
    );

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
});

builder.Services.AddTransient<IMovieService, MovieServiceImpl>();
builder.Services.AddTransient<IFranchiseService, FranchiseServiceImpl>();
builder.Services.AddTransient<ICharacterService, CharacterServiceImpl>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
