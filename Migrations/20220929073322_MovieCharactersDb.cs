﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Assignment_03_NET.Migrations
{
    public partial class MovieCharactersDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", maxLength: 10, nullable: false),
                    Director = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Obi-Wan", "Obi-Wan Kenobi", "Male", "https://premium.vgc.no/v2/images/969085ab-1aab-4786-ad32-2729e48a7894?fit=crop&h=952&w=1129&s=89c36cb2d721fa69f7c209e6eab9bb0705dcd374" },
                    { 2, "Anakin", "Anakin Skywalker", "Male", "https://static.wikia.nocookie.net/starwars/images/5/59/Anakin_skywalker.jpg/revision/latest?cb=20080428120001&path-prefix=no" },
                    { 3, "Iron Man", "Tony Stark", "Male", "https://upload.wikimedia.org/wikipedia/en/f/f2/Robert_Downey_Jr._as_Tony_Stark_in_Avengers_Infinity_War.jpg" },
                    { 4, "Ant-Man", "Scott Lang", "Male", "https://upload.wikimedia.org/wikipedia/en/thumb/8/88/Paul_Rudd_as_Ant-Man.jpg/250px-Paul_Rudd_as_Ant-Man.jpg" },
                    { 5, "Dr. Strange", "Stephen Stange", "Male", "https://static.wikia.nocookie.net/disney/images/d/dc/Doctor_Strange_-_Profile.png/revision/latest?cb=20220804200852" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "An American epic space opera multimedia franchise, created by George Lucas.", "Star Wars" },
                    { 2, "Marvel is a set of superhero movies based on Stan Lee's comic books.", "Marvel" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "George Lucas", 1, "Science Fiction", "Episode 1 - The Phantom Manace", "https://upload.wikimedia.org/wikipedia/en/4/40/Star_Wars_Phantom_Menace_poster.jpg", 1999, "https://www.youtube.com/watch?v=bD7bpG-zDJQ" },
                    { 2, "George Lucas", 1, "Science Fiction", "Episode 2 - Attack of the Clones", "https://upload.wikimedia.org/wikipedia/en/3/32/Star_Wars_-_Episode_II_Attack_of_the_Clones_%28movie_poster%29.jpg", 2002, "https://www.youtube.com/watch?v=gYbW1F_c9eM" },
                    { 3, "George Lucas", 1, "Science Fiction", "Episode 3 - The Revenge of the Sith", "https://upload.wikimedia.org/wikipedia/en/9/93/Star_Wars_Episode_III_Revenge_of_the_Sith_poster.jpg", 2005, "https://www.youtube.com/watch?v=5UnjrG_N8hU" },
                    { 4, "Peyton Reed", 2, "Action, Adventure, Comedy", "Ant-Man", "https://m.media-amazon.com/images/M/MV5BMjM2NTQ5Mzc2M15BMl5BanBnXkFtZTgwNTcxMDI2NTE@._V1_FMjpg_UX1000_.jpg", 2015, "https://www.youtube.com/watch?v=pWdKf3MneyI" },
                    { 5, "Scott Derrickson", 2, "Action, Adventure, Fantasy", "Doctor Strange", "https://m.media-amazon.com/images/M/MV5BNjgwNzAzNjk1Nl5BMl5BanBnXkFtZTgwMzQ2NjI1OTE@._V1_.jpg", 2016, "https://www.youtube.com/watch?v=h7gvFravm4A" },
                    { 6, "Jon Favreau", 2, "Action, Adventure, Science Fiction", "Iron-Man", "https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_FMjpg_UX1000_.jpg", 2008, "https://www.youtube.com/watch?v=KAE5ymVLmZg" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 1, 3 },
                    { 2, 3 },
                    { 4, 4 },
                    { 5, 5 },
                    { 3, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_CharacterId",
                table: "CharacterMovie",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
