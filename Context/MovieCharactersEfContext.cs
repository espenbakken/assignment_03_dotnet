﻿using Assignment_03_NET.Models;
using Microsoft.EntityFrameworkCore;

namespace Assignment_03_NET.Context
{
    public class MovieCharactersEfContext : DbContext
    {
        public MovieCharactersEfContext(DbContextOptions<MovieCharactersEfContext> options) : base(options)
        {
        }

        public MovieCharactersEfContext()
        {
        }

        public virtual DbSet<Movie> Movies { get; set; } = null!;
        public virtual DbSet<Franchise> Franchises { get; set; } = null!;
        public virtual DbSet<Character> Characters { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region New movies 
            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    Id = 1,
                    MovieTitle = "Episode 1 - The Phantom Manace",
                    Director = "George Lucas",
                    FranchiseId = 1,
                    Genre = "Science Fiction",
                    ReleaseYear = 1999,
                    Picture = "https://upload.wikimedia.org/wikipedia/en/4/40/Star_Wars_Phantom_Menace_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=bD7bpG-zDJQ",
                },
                new Movie()
                {
                    Id = 2,
                    MovieTitle = "Episode 2 - Attack of the Clones",
                    Director = "George Lucas",
                    FranchiseId = 1,
                    Genre = "Science Fiction",
                    ReleaseYear = 2002,
                    Picture = "https://upload.wikimedia.org/wikipedia/en/3/32/Star_Wars_-_Episode_II_Attack_of_the_Clones_%28movie_poster%29.jpg",
                    Trailer = "https://www.youtube.com/watch?v=gYbW1F_c9eM",
                },
                new Movie()
                {
                    Id = 3,
                    MovieTitle = "Episode 3 - The Revenge of the Sith",
                    Director = "George Lucas",
                    FranchiseId = 1,
                    Genre = "Science Fiction",
                    ReleaseYear = 2005,
                    Picture = "https://upload.wikimedia.org/wikipedia/en/9/93/Star_Wars_Episode_III_Revenge_of_the_Sith_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=5UnjrG_N8hU",
                },
                new Movie()
                {
                    Id = 4,
                    MovieTitle = "Ant-Man",
                    Director = "Peyton Reed",
                    FranchiseId = 2,
                    Genre = "Action, Adventure, Comedy",
                    ReleaseYear = 2015,
                    Picture = "https://m.media-amazon.com/images/M/MV5BMjM2NTQ5Mzc2M15BMl5BanBnXkFtZTgwNTcxMDI2NTE@._V1_FMjpg_UX1000_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=pWdKf3MneyI",
                },
                new Movie()
                {
                    Id = 5,
                    MovieTitle = "Doctor Strange",
                    Director = "Scott Derrickson",
                    FranchiseId = 2,
                    Genre = "Action, Adventure, Fantasy",
                    ReleaseYear = 2016,
                    Picture = "https://m.media-amazon.com/images/M/MV5BNjgwNzAzNjk1Nl5BMl5BanBnXkFtZTgwMzQ2NjI1OTE@._V1_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=h7gvFravm4A",
                },
                new Movie()
                {
                    Id = 6,
                    MovieTitle = "Iron-Man",
                    Director = "Jon Favreau",
                    FranchiseId = 2,
                    Genre = "Action, Adventure, Science Fiction",
                    ReleaseYear = 2008,
                    Picture = "https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_FMjpg_UX1000_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=KAE5ymVLmZg",
                }
                );
            #endregion

            #region New franchises
            modelBuilder.Entity<Franchise>()
                .HasMany(f => f.Movies)
                .WithOne(m => m.Franchise)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Franchise>().HasData(
                new Franchise()
                {
                    Id = 1,
                    Name = "Star Wars",
                    Description = "An American epic space opera multimedia franchise, created by George Lucas.",
                },
                new Franchise()
                {
                    Id = 2,
                    Name = "Marvel",
                    Description = "Marvel is a set of superhero movies based on Stan Lee's comic books."
                }
                );
            #endregion

            #region New characters
            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 1,
                    FullName = "Obi-Wan Kenobi",
                    Alias = "Obi-Wan",
                    Gender = "Male",
                    Picture = "https://premium.vgc.no/v2/images/969085ab-1aab-4786-ad32-2729e48a7894?fit=crop&h=952&w=1129&s=89c36cb2d721fa69f7c209e6eab9bb0705dcd374"
                },
                new Character()
                {
                    Id = 2,
                    FullName = "Anakin Skywalker",
                    Alias = "Anakin",
                    Gender = "Male",
                    Picture = "https://static.wikia.nocookie.net/starwars/images/5/59/Anakin_skywalker.jpg/revision/latest?cb=20080428120001&path-prefix=no"
                },
                new Character()
                {
                    Id = 3,
                    FullName = "Tony Stark",
                    Alias = "Iron Man",
                    Gender = "Male",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/f/f2/Robert_Downey_Jr._as_Tony_Stark_in_Avengers_Infinity_War.jpg"
                },
                new Character()
                {
                    Id = 4,
                    FullName = "Scott Lang",
                    Alias = "Ant-Man",
                    Gender = "Male",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/8/88/Paul_Rudd_as_Ant-Man.jpg/250px-Paul_Rudd_as_Ant-Man.jpg"
                },
                new Character()
                {
                    Id = 5,
                    FullName = "Stephen Stange",
                    Alias = "Dr. Strange",
                    Gender = "Male",
                    Picture = "https://static.wikia.nocookie.net/disney/images/d/dc/Doctor_Strange_-_Profile.png/revision/latest?cb=20220804200852"
                }
                );
            #endregion

            #region CharacterMovie
            // Connection between movie and characters
            modelBuilder.Entity<Character>()
                .HasMany(character => character.Movies)
                .WithMany(movie => movie.Characters)
                .UsingEntity<Dictionary<string, object>>
                (
                    "CharacterMovie",
                    right => right.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    left => left.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    characterMovie =>
                    {
                        characterMovie.HasKey("MovieId", "CharacterId");
                        characterMovie.HasData
                        (
                            //Star Wars
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 1, MovieId = 3 },
                            new { CharacterId = 2, MovieId = 3 },
                            //Marvel
                            new { CharacterId = 3, MovieId = 6 },
                            new { CharacterId = 4, MovieId = 4 },
                            new { CharacterId = 5, MovieId = 5 }
                        );
                    }
                 );
            #endregion

        }
    }
}
