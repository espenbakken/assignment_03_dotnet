## Name
Assignment 6 Web API

## Description
This project consist of a Web API with CRUD functionality. Project is written in .NET 6.

## Installation
Clone this project. Make sure you have .NET 6 installed, and SSMS installed. Please alter the ConnectionString in appsettings.json to your own ConnectionString. Please run these commands in NuGet Package Manager Console:
```
Add-Migration MovieCharactersDb
```
```
update-database
```

## Usage
This Project can be run with Run-command in your IDE, or by using:
```
dotnet build --no-restore
```
in the terminal.

## Contributors
Espen Bakken

Kristian Stoa Mathisen

## License
Use as you wish in your own repository. Please do not override excisting repo. 

